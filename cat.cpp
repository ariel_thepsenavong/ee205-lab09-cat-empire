///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   04_27_21
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

bool CatEmpire::empty(){   //return true when tree is empty
   if(topCat == nullptr)
      return true;
   else
      return false;
}

void CatEmpire::addCat(Cat* newCat){
   if(newCat == nullptr)
      return;

   newCat -> left = nullptr;
   newCat -> right = nullptr;

   if(topCat == nullptr){
      topCat = newCat;
      return;
   }

   CatEmpire::addCat(topCat, newCat);
}

void CatEmpire::addCat(Cat* atCat, Cat* newCat){   //internal helper function
   assert(atCat != nullptr);
   assert(newCat != nullptr);

   //if atCat > newCat, atCat -> RIGHT of newCat
   if(atCat -> name > newCat -> name){
      if(atCat -> left == nullptr){
         atCat -> left = newCat;
      }
      else{
         CatEmpire::addCat(atCat -> left, newCat);
      }
   }
   
   //if atCat < newCat, atCat -> LEFT of newCat
   if(atCat -> name < newCat -> name){
      if(atCat -> right == nullptr){
         atCat -> right = newCat;
      }
      else{
         CatEmpire::addCat(atCat -> right, newCat);
      }
   }
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}

void CatEmpire::dfsInorderReverse(Cat* atCat, int depth) const{
   if(topCat == nullptr)
      return;

   Cat* prevCat;
   for(atCat = topCat; atCat != nullptr;){
      if(atCat-> right == nullptr){
        depth++;
        cout << string (6 * (depth-1), ' ' ) << atCat -> name << endl;
        atCat = atCat -> left;
        depth++;
        continue;
      }
      for(prevCat = atCat -> right; prevCat -> left != nullptr && prevCat -> left != atCat; prevCat = prevCat -> left){
      }
      if(prevCat -> left == nullptr){
         prevCat -> left = atCat; 
         atCat = atCat -> right;
         depth++;
         continue;
      }
      else{
         prevCat-> left = nullptr;
         //depth--;
         cout << string (6 * (depth-1), ' ' ) << atCat -> name;
         if(atCat -> left == nullptr && atCat -> right == nullptr){
            cout << endl;
            depth--;
         }
         if(atCat-> left != nullptr && atCat -> right != nullptr){
            cout << " < " << endl;
           // depth--;
         }
         //depth++;
         if(atCat -> left != nullptr){ //&& atCat -> right == nullptr){
            cout << "/ " << endl;
            depth++;
         }

         if(atCat -> right != nullptr){ //&& atCat -> left == nullptr){
            cout << "\\" << endl;
            depth++;
         }
         atCat = atCat -> left;
         continue;
      }
   }
   //CatEmpire::dfsInorderReverse(atCat -> right, depth);
   //cout << atCat << endl;

   //CatEmpire::dfsInorderReverse(atCat -> left, depth);
   /*if(atCat -> right != nullptr && atCat -> right -> right != nullptr){
      atCat = atCat -> right;
      depth ++;
      //cout << depth << endl;
     
      CatEmpire::dfsInorderReverse(atCat, depth);

      if(atCat -> right != nullptr){   //If right of atCat has something
        depth++;
        cout << string (6 * (depth-1), ' ' ) << atCat -> right -> name;
        if(atCat -> right -> right == nullptr && atCat -> left != nullptr){    //If right of atCat is nullptr
            cout << endl;
            depth --;
        }
        
        if(atCat -> right != nullptr && atCat -> left != nullptr){
            cout << string (6 * (depth-1), ' ' ) << atCat -> name;
            cout << " < " << endl;
            atCat = atCat -> left;
            depth++;
         }

            CatEmpire::dfsInorderReverse(atCat, depth); 
      }
   }

   else{
      if(atCat -> right != nullptr && atCat -> left != nullptr){
         depth++;
         cout << string (6 * (depth-1), ' ' ) << atCat -> right -> name;
         cout << "\\ " << endl;

         if(atCat -> right -> right == nullptr){
            depth++;

            if(atCat -> right -> left -> left == nullptr){
               depth++;
               cout << string (6 * (depth-1), ' ' ) << atCat -> right -> left -> right -> name;
               cout << endl;
               depth--;
            }
            
            cout << string (6 * (depth-1), ' ' ) << atCat -> right -> left -> name;
            cout << "/" << endl;
            depth--;
         }
         
         cout << string (6 * (depth-1), ' ' ) << atCat -> name;
         cout << "<" << endl;
         depth++;
         cout << string (6 * (depth-1), ' ' ) << atCat -> left -> name;
         cout << endl;

      }
   }

   if(atCat -> left -> left == nullptr && atCat -> right != nullptr){
      atCat = atCat -> right;
      depth++;
      CatEmpire::dfsInorderReverse(atCat, depth);
   }
*/
/*   atCat = topCat;
   depth = 1;
   cout << string (6 * (depth-1), ' ' ) << atCat -> name;
   cout << "< " << endl; 
   atCat = atCat -> right;
   CatEmpire:: dfsInorderReverse(atCat, depth);
*/
}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}

void CatEmpire::dfsInorder(Cat* atCat) const{
   if(atCat == nullptr)
      return;

   if(atCat -> left != nullptr && atCat -> left -> left != nullptr){
      atCat = atCat -> left;

      if(atCat -> left != nullptr){
         cout << atCat -> left -> name << endl;
         
         if(atCat -> left -> left == nullptr){
            cout << atCat -> name << endl;
            atCat = atCat -> right;
            if(atCat -> right != nullptr)
               cout << atCat -> name << endl;
         }

         CatEmpire::dfsInorder(atCat);
      }
   }
   else{  

      if(atCat -> left != nullptr && atCat -> right == nullptr){
         cout << atCat -> left -> name << endl;
         cout << atCat -> left -> right -> name << endl;
         cout << atCat -> name << endl;
         cout << topCat -> right -> name << endl;
         cout << topCat -> right -> right -> name << endl;
      }

      if(atCat -> right -> right == nullptr){
         cout << atCat -> right -> name << endl;
      }

      atCat = atCat -> right;
   
      if(atCat -> left != nullptr && atCat -> right != nullptr){
         cout << atCat -> left -> name << endl;
         cout << atCat -> name << endl;
         CatEmpire::dfsInorder(atCat);
      }
  
   }

   atCat = topCat;
   cout << atCat -> name << endl;
   atCat = atCat -> right;
   CatEmpire::dfsInorder(atCat);
   return; 
}


void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

void CatEmpire::dfsPreorder(Cat* atCat) const{
   if(atCat == nullptr)
      return;

   if(atCat -> left == nullptr && atCat -> right == nullptr)   //Cat has no child
      CatEmpire::dfsPreorder(atCat -> right);

   if(atCat -> right != nullptr && atCat -> left != nullptr){  //Cat has both left and right child
      
         cout << atCat -> name << " begat " << atCat->left-> name << " and " << atCat -> right -> name << endl;
         
         if(atCat -> name > atCat -> left -> name && atCat -> left -> left != nullptr){
         atCat = atCat -> left; 
         CatEmpire::dfsPreorder(atCat);
         
         }
         else{
            atCat = atCat -> right;
            
            if(atCat -> right == nullptr && atCat -> left == nullptr){
               atCat = topCat -> right;
               cout << atCat -> name << " begat " << atCat->left-> name << " and " << atCat -> right -> name << endl;
               atCat = atCat -> left;
               CatEmpire::dfsPreorder(atCat); 
            }
            else{
            //if(atCat -> right == nullptr && atCat -> left != nullptr){
               CatEmpire::dfsPreorder(atCat);
            }
            CatEmpire::dfsPreorder(atCat);
         
         }

         CatEmpire::dfsPreorder(topCat -> right);
   } 

   if(atCat -> left == nullptr && atCat -> right != nullptr){  //Cat has only right child
      cout << atCat -> name << " begat " << atCat -> right -> name << endl;

      if(atCat -> left == nullptr && atCat -> right -> right == nullptr){
         atCat = atCat -> left;
         cout << atCat -> name << endl;
         CatEmpire::dfsPreorder(atCat);
         return;
      }
      atCat = atCat -> right;
      CatEmpire::dfsPreorder(atCat);
   }

   if(atCat -> left != nullptr && atCat -> right == nullptr){  //Cat has only left child
      cout << atCat -> name << " begat " << atCat -> left -> name << endl;
      atCat = atCat -> left;
      CatEmpire::dfsPreorder(atCat);
   }


}

void CatEmpire::catGenerations() const{
  int depth = 1;

   if(topCat == nullptr)
      return;
   queue<Cat*> catQueue;
   
   Cat* cat;
   catQueue.push(topCat);
   catQueue.push(nullptr);

   CatEmpire::getEnglishSuffix(depth);
   depth++;

   while(catQueue.size() > 1){
      cat = catQueue.front();
      catQueue.pop();
      if(cat == nullptr){
         catQueue.push(nullptr);
         cout << endl;
         CatEmpire::getEnglishSuffix(depth);
         depth++;
      }
      else{
         if(cat -> left){
            catQueue.push(cat -> left);
         }
         if(cat -> right){
            catQueue.push(cat -> right);
         }

         cout << cat -> name << " ";
      }
   }
}

void CatEmpire::getEnglishSuffix(int n) const{
   if(n == 1)
      cout << "1st Generation" << endl;

   if(n == 2)
      cout << "2nd Generation" << endl;

   if(n==3)
      cout << "3rd Generation" << endl;

   if(n>3){
      cout << n;
      cout << "th Generation" << endl;
   }

}
